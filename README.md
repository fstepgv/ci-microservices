# Gitlab CI microservices project

## Description


Microservices  emulation project made up of 1 frontend service and 2 backend services: products & shopping-cart.

All microservices are being build and deployed to AWS EC-2 instance.

Each service start separately and has no dependencies to other.

Services are connected together to one docker network via Docker compose.

The pipeline starts only for the modified service .

Docker compose file is tweaked , that it stops/start only the service, which has trigerred a pipeline.







